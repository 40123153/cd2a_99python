#coding: utf-8
'''
用來導入 Brython 網際運算環境


'''
def BrythonConsole():
    return '''
<script src="/Brython1.2-20131109-201900/brython.js"></script>
<script>
window.onload = function(){
    brython(1);
}
</script><script type="text/python">
import sys
import time
import dis

if sys.has_local_storage:
    from local_storage import storage
else:
    storage = False

def reset_src():
    if storage:
        doc['src'].value = storage["py_src"]

def write(data):
    doc["console2"].value += str(data)

#sys.stdout = object()    #not needed when importing sys via src/Lib/sys.py
sys.stdout.write = write

def to_str(xx):
    return str(xx)

doc['version'].text = '.'.join(map(to_str,sys.version_info))

# 配合 20130817 版本, 改為下列流程
def write(data):
    doc["console2"].value += str(data)
sys.stdout.write = write
sys.stderr.write = write
# 結束 20130817 版本修改

output = ''

def show_console2():
    doc["console2"].value = output
    doc["console2"].cols = 60

def clear_text():
    log("event clear")
    doc['console2'].value=''
    #doc['src'].value=''

def clear_canvas():
    canvas = doc["plotarea"]
    ctx = canvas.getContext("2d")
    ctx.clearRect(0, 0, canvas.width, canvas.height)

def run():
    global output
    doc["console2"].value=''
    doc["console2"].cols = 60
    src = doc["src"].value
    if storage:
        storage["py_src"]=src
    t0 = time.time()
    exec(src)
    output = doc["console2"].value
    print('<done in %s ms>' %(time.time()-t0))

def show_js():
    src = doc["src"].value
    doc["console2"].cols = 90
    doc["console2"].value = dis.dis(src)

</script>
<table width=80%>
<tr><td style="text-align:center"><b>Python</b>
</td>
<td>&nbsp;</td>
<th><input type="button" value="Console" onClick="show_console2()"></th>
<th><input type="button" value="Javascript" onClick="show_js()"></th>
</tr>
<tr><td><div id="editor"></div><textarea id="src" name=form_34ed066df378efacc9b924ec161e7639program cols="60" rows="20">
for i in range(1,10):
    print("這是",i," x 1~9")
    for j in range(1,10):
        c = i*j
        print(c)
    print("\n")
</textarea></td><td><input type="button" value="Run" onClick="run()"></td>
<td><input type="button" value="Clear Output" onClick="clear_text()">
<input type="button" value="Clear Canvas" onClick="clear_canvas()">
</td>
<td colspan=2><textarea id="console2" cols="60" rows="20"></textarea></td>
</tr>
<tr><td colspan=2>
<p>Brython version <span id="version"></td>
</tr>
<tr><td colspan="4">
<div id="outputdiv"></div>
<textarea id="dataarea" cols=30 rows=5></textarea>
</td>
</tr>
<tr><td colspan="4">
<canvas id="plotarea" width="640" height="640"></canvas>
</td>
</tr>
</table>
'''