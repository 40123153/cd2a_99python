#coding: utf-8
'''
用來導入 Brython 網際運算環境


'''
def BrythonConsole():
    return '''
<script src="/Brython1.2-20131109-201900/brython.js"></script>
<script>
window.onload = function(){
    brython(1);
}
</script><script type="text/python">
import sys
import time
import dis

if sys.has_local_storage:
    from local_storage import storage
else:
    storage = False

def reset_src():
    if storage:
        doc['src'].value = storage["py_src"]

def write(data):
    doc["console2"].value += str(data)

#sys.stdout = object()    #not needed when importing sys via src/Lib/sys.py
sys.stdout.write = write

def to_str(xx):
    return str(xx)

doc['version'].text = '.'.join(map(to_str,sys.version_info))

# 配合 20130817 版本, 改為下列流程
def write(data):
    doc["console2"].value += str(data)
sys.stdout.write = write
sys.stderr.write = write
# 結束 20130817 版本修改

output = ''

def show_console2():
    doc["console2"].value = output
    doc["console2"].cols = 60

def clear_text():
    log("event clear")
    doc['console2'].value=''
    #doc['src'].value=''

def clear_canvas():
    canvas = doc["plotarea"]
    ctx = canvas.getContext("2d")
    ctx.clearRect(0, 0, canvas.width, canvas.height)

def run():
    global output
    doc["console2"].value=''
    doc["console2"].cols = 60
    src = doc["src"].value
    if storage:
        storage["py_src"]=src
    t0 = time.time()
    exec(src)
    output = doc["console2"].value
    print('<done in %s ms>' %(time.time()-t0))

def show_js():
    src = doc["src"].value
    doc["console2"].cols = 90
    doc["console2"].value = dis.dis(src)

</script>
<table width=80%>
<tr><td style="text-align:center"><b>Python</b>
</td>
<td>&nbsp;</td>
<th><input type="button" value="Console" onClick="show_console2()"></th>
<th><input type="button" value="Javascript" onClick="show_js()"></th>
</tr>
<tr><td><div id="editor"></div><textarea id="src" name=form_34ed066df378efacc9b924ec161e7639program cols="60" rows="20">import cherrypy
import os

# 確定程式檔案所在目錄
_curdir = os.path.join(os.getcwd(), os.path.dirname(__file__))

class HelloWorld(object):
    def index(self):
        return '''
<script src="/static/jscript/pfcUtils.js">
</script><script src="/static/jscript/wl_header.js">
document.writeln ("Error loading Pro/Web.Link header!");
</script><script language="JavaScript">
if (!pfcIsWindows()) netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
// 若第三輸入為 false, 表示僅載入 session, 但是不顯示
// ret 為 model open return
 var ret = document.pwl.pwlMdlOpen("cadp_block.prt", "c:/tmp", false);
if (!ret.Status) {
    alert("pwlMdlOpen failed (" + ret.ErrorCode + ")");
}
    //將 ProE 執行階段設為變數 session
    var session = pfcGetProESession();
    // 在視窗中打開零件檔案, 並且顯示出來
    var window = session.OpenFile(pfcCreate("pfcModelDescriptor").CreateFromFileName("cadp_block.prt"));
    var solid = session.GetModel("cadp_block.prt",pfcCreate("pfcModelType").MDL_PART);
    var d1,d2,myf,myn,i,j,volume,count,d1Value,d2Value;
    // 將模型檔中的 diameter 變數設為 javascript 中的 diameter 變數
    d1 = solid.GetParam("length");
    // 將模型檔中的 height 變數設為 javascript 中的 height 變數
    d2 = solid.GetParam("width");
//改變零件尺寸
    //myf=20;
    //myn=20;
    volume=0;
    count=0;
    try
    {
            // 以下採用 URL 輸入對應變數
            //createParametersFromArguments ();
            // 以下則直接利用 javascript 程式改變零件參數
            for(i=0;i<=3;i++)
            {
                //for(j=0;j<=2;j++)
                //{
                    myf=150;
                    myn=100+i*10;
// 設定變數值, 利用 ModelItem 中的 CreateDoubleParamValue 轉換成 Pro/Web.Link 所需要的浮點數值
         d1Value = pfcCreate ("MpfcModelItem").CreateDoubleParamValue(myf);
         d2Value = pfcCreate ("MpfcModelItem").CreateDoubleParamValue(myn);
// 將處理好的變數值, 指定給對應的零件變數
                    d1.Value = d1Value;
                    d2.Value = d2Value;
                    //零件尺寸重新設定後, 呼叫 Regenerate 更新模型
                    //solid.Regenerate(void null);
                    //利用 GetMassProperty 取得模型的質量相關物件
                    properties = solid.GetMassProperty(void null);
                    //volume = volume + properties.Volume;
volume = properties.Volume;
                    count = count + 1;
alert("執行第"+count+"次,零件總體積:"+volume);
// 將零件存為新檔案
var newfile = document.pwl.pwlMdlSaveAs("cadp_block.prt", "c:/tmp", "cadp_block_"+count+".prt");
if (!newfile.Status) {
    alert("pwlMdlSaveAs failed (" + newfile.ErrorCode + ")");
//}
                }
            }
            //alert("共執行:"+count+"次,零件總體積:"+volume);
            //alert("零件體積:"+properties.Volume);
            //alert("零件體積取整數:"+Math.round(properties.Volume));
        }
    catch(err)
        {
            alert ("Exception occurred: "+pfcGetExceptionType (err));
        }
</script>
'''
    index.exposed = True

    @cherrypy.expose
    def index2(self):
        return '''
<script src="/static/jscript/pfcUtils.js">
</script><script  src="/static/jscript/pfcParameterExamples.js"></script><script  src="/static/jscript/pfcComponentFeatExamples.js">
 document.writeln ("Error loading script!");
</script><script language="JavaScript">
      if (!pfcIsWindows())
        netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
  var session = pfcGetProESession ();
// for volume
  var solid = session.CurrentModel;
	try
		{
			createParametersFromArguments ();
                     solid.Regenerate(void null);   
                        properties = solid.GetMassProperty(void null);
                        alert("零件體積:"+properties.Volume);
		}
	catch (err)
		{
			alert ("Exception occurred: "+pfcGetExceptionType (err));
		}
</script>
'''
# 配合程式檔案所在目錄設定靜態目錄或靜態檔案
application_conf = {'/static':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': _curdir+"/static"}
    }

#cherrypy.quickstart(HelloWorld(), config=application_conf)
application=cherrypy.Application(HelloWorld(), config=application_conf)</textarea></td><td><input type="button" value="Run" onClick="run()"></td>
<td><input type="button" value="Clear Output" onClick="clear_text()">
<input type="button" value="Clear Canvas" onClick="clear_canvas()">
</td>
<td colspan=2><textarea id="console2" cols="60" rows="20"></textarea></td>
</tr>
<tr><td colspan=2>
<p>Brython version <span id="version"></td>
</tr>
<tr><td colspan="4">
<div id="outputdiv"></div>
<textarea id="dataarea" cols=30 rows=5></textarea>
</td>
</tr>
<tr><td colspan="4">
<canvas id="plotarea" width="640" height="640"></canvas>
</td>
</tr>
</table>
'''